const babel = require('gulp-babel');
const eslint = require('gulp-eslint');
const flow = require('gulp-flowtype');
const gulp = require('gulp');
const gulpDebug = require('gulp-debug');
const gulpIf = require('gulp-if');
const gulpUtil = require('gulp-util');
const path = require('path');
const terser = require('gulp-terser');
const webpack = require('webpack');
const merge = require('webpack-merge');

const webpackConfig = require('./webpack.config');
const babelConfig = require('./babel.config');

// Create a babel configuration that operates over legacy JS code with no explicit processing,
// for src/main/resources/js

const legacyBabelConfig = merge(babelConfig, {
    presets: [['@babel/preset-env', {modules: false}]]
});

// Create a babel configuration that automatically wraps legacy code using ES imports and
// transforms into AMD modules.

const modulesBabelConfig = merge(babelConfig, {
    presets: [['@babel/preset-env', { modules: 'amd' }]],
    moduleIds: true,
    sourceRoot: 'devdayapp2019/modules' // defines the prefix used when require()ing the module
});

// Legacy JS is in src/main/resources/js

const LEGACY_JS_SRC = path.join(__dirname, 'src', 'main', 'resources', 'js', '**', '*.js');
const LEGACY_JS_OUTPUT_DIR = path.join('target', 'classes', 'js');

// Legacy JS with auto-AMD transformation in src/main/resources/modules

const LEGACY_JS_MODULES_SRC = path.join(__dirname, 'src', 'main', 'resources', 'modules', '**', '*.js');
const LEGACY_JS_MODULES_OUTPUT_DIR = path.join('target', 'classes', 'modules');

// Use NODE_ENV to tell us whether to run in production mode or not

const PRODUCTION = (process.env.NODE_ENV==='production');

// Run webpack on an array of webpack config objects

const runWebpack = (configAry, watching, callback) => {
    // Set watching: true if we are in a watch task
    configAry = configAry.map(oneConfig => merge(oneConfig, { watch: watching }));

    webpack(configAry, (err, stats) => {
        if (err) {
            gulpUtil.log('[webpack error]', err);
        } else {
            gulpUtil.log('[webpack]', stats.toString());
        }

        if (!watching) {
            callback();
        }
    });

    return null;
};

// Generic task for processing standalone .JS files
//

const processJs = (src, dst, babelConfig, watching) => {
    return gulp.src(src)
        .pipe(flow())
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
        .pipe(babel(babelConfig))
        .pipe(gulpIf(PRODUCTION, terser()))
        .pipe(gulp.dest(dst))
        .pipe(gulpDebug({title: 'output'}));
};

// Now bind specific versions of our JS pipeline to run on different source trees, with different babel configs

const processLegacyJs = processJs.bind(null, LEGACY_JS_SRC, LEGACY_JS_OUTPUT_DIR, legacyBabelConfig);
const processModulesJs = processJs.bind(null, LEGACY_JS_MODULES_SRC, LEGACY_JS_MODULES_OUTPUT_DIR, modulesBabelConfig);

const watchLegacyJs = () =>     gulp.watch(LEGACY_JS_SRC,          processLegacyJs.bind(null, true));
const watchModulesJs = () =>    gulp.watch(LEGACY_JS_MODULES_SRC,  processModulesJs.bind(null, true));

// If we were implementing tests, they'd go here.

const noTests = (callback) => {
    callback();
};

// When we run a build, execute webpack, legacy JS and module JS all in parallel

const build = gulp.parallel([
    runWebpack.bind(null, webpackConfig, false),
    processLegacyJs.bind(null, false),
    processModulesJs.bind(null, false)
]);

// Similar for watch

const watch = gulp.parallel([
    runWebpack.bind(null, webpackConfig, true),
    watchLegacyJs,
    watchModulesJs
]);

const test = gulp.series([ noTests ]);

gulp.task('build', build);
gulp.task('watch', watch);
gulp.task('test', test);