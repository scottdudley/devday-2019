module.exports = {
    plugins: [
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-transform-runtime'
    ],
    presets: [
        // Appropriate preset-env is applied dynamically to this config from gulpfile.js, with supported
        // "browserslist" in package.json.

        '@babel/preset-react',
        '@babel/preset-flow'
    ]
};