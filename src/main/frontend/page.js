// @flow

import * as React from 'react';
import Select from '@atlaskit/select';
import CheckCircle from '@atlaskit/icon/glyph/check-circle';
import Spinner from '@atlaskit/spinner';
import styled from 'styled-components';
import Banner from '@atlaskit/banner';
import { colors } from '@atlaskit/theme';
import Button from '@atlaskit/button';

// References to legacy, manually-defined AMD modules
import legacyCode from 'devdayapp2019/legacyCode';

// References to legacy code as ES modules
import * as legacyModule from 'devdayapp2019/modules/legacyModule';

// Resources
import './css/test.css';
import atlassianLogo from './images/atlassian-logo.png';

const LENTILS = {value: "lentils", label: "Lentils"};
const CHICKEN = {value: "chicken", label: "Chicken"};
const BEEF = {value: "beef", label: "Beef"};
const CORN = {value: "corn", label: "Corn"};

const NarrowSelect = styled(Select)`
    max-width: 200px;
    margin: 15px 0;
`;

const Padded = styled.div`
    margin: 15px 0;
`;

const Icon = (<CheckCircle label="Success" primaryColor={colors.G300} />);

const SuccessBanner = () => (
    <Banner icon={Icon} isOpen={true} appearance="warning">
        Hello, banner with icon!
    </Banner>);

type Props = {}


class Page extends React.Component<Props> {
    render() {
        // We can call imported global functions from legacy code

        window.demoCallToBadGlobalFunction('content');

        return (
            <div>
                <h3>AtlasKit components</h3>

                <Padded>
                    <Spinner />
                </Padded>

                <Padded>
                    <SuccessBanner/>
                </Padded>

                <Padded>
                    <Button appearance="primary">Click me</Button>
                </Padded>

                <Padded>
                    Food:
                    <NarrowSelect
                        options={[LENTILS, CHICKEN, BEEF, CORN]}
                        defaultValue={CORN}
                    />
                </Padded>

                <h3>{"Let's look at some I18n text"}</h3>

                {AJS.I18n.getText('devday2019.main.title', '<my dynamic i18n arg>')}

                <h3>Invoking some legacy code</h3>

                <p>1 + 2 = {legacyCode.adder(1, 2)}</p>

                <h3>Invoking some legacy code as an ES module </h3>

                <p>3 + 4 = {legacyModule.moduleAdder(3, 4)}</p>

                <h3>Showing off some CSS</h3>

                <span className="testCss">I should have a red border</span>

                <h3>Embedded images</h3>

                <p>As an imported IMG: <img src={atlassianLogo} width="100" height="12" alt="Atlassian"/></p>

            </div>);
    }
}

export default Page;