// @flow

import './polyfill';
import * as React from 'react';
import ReactDOM from 'react-dom';
import Page from './page';

AJS.toInit(() => {
    const mount = document.querySelector('.reactMount');
    mount && ReactDOM.render((<Page/>), mount);
});