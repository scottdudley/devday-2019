// @flow

const moduleAdder = (a: number, b: number) => {
    return a + b;
};

export { moduleAdder };