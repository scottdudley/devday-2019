// @flow

// Manually-defined code with no types and manual AMD

define('devdayapp2019/legacyCode', [], function () {
    return {
        adder: (a, b) => {
            return a + b;
        }
    };
});

// We also declare an evil global function

window.demoCallToBadGlobalFunction = function(str) {
    console.log('Do some stuff: ' + str); //eslint-disable-line no-console
    return 'Evil';
};