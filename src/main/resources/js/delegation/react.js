// @flow

// Export the host product 'react' as 'devday2019/react'. See comments in atlassian-plugin.xml

define('devday2019/react', ['react'], function (react) {
    return react;
});