// @flow

// Export the host product 'react-dom' as 'devday2019/react-dom'. See comments in atlassian-plugin.xml

define('devday2019/react-dom', ['react-dom'], function (reactDom) {
    return reactDom;
});