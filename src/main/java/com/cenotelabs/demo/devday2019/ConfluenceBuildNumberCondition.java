package com.cenotelabs.demo.devday2019;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.sal.api.ApplicationProperties;

import java.util.Map;

/**
 * The Confluence-internal version of this class refuses to autowire itself properly in
 * Confluence 6.15, leaving us with a NPE. Instead, we implement this ourselves and ensure
 * that we work around CONFSERVER-43542 while we are at it.
 */
public class ConfluenceBuildNumberCondition implements UrlReadingCondition
{
    @ComponentImport
    private ApplicationProperties applicationProperties;
    private boolean show;

    public ConfluenceBuildNumberCondition()
    {
    }

    public void setApplicationProperties(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {
        String minBuildNumber = params.get("minBuildNumber");
        int minBuild = Integer.parseInt(minBuildNumber);
        int currentBuild = Integer.parseInt(this.applicationProperties.getBuildNumber());

        this.show = currentBuild >= minBuild;
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder)
    {
    }

    @Override
    public boolean shouldDisplay(QueryParams queryParams)
    {
        return this.show;
    }
}