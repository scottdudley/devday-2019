require('eslint-loader');
const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const FlowWebpackPlugin = require('flow-webpack-plugin');
const globEntries = require('webpack-glob-entries');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const { default : babelConfig } = require('./babel.config');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const merge = require('webpack-merge');

const PLUGIN_KEY = 'com.cenotelabs.demo.devday-2019';
const WEBPACK_JSONP_FUNCTION_NAME = 'devDay2019Jsonp'; // IMPORTANT! Must be a unique globally-namespaced for your app!
const PRODUCTION = (process.env.NODE_ENV==='production');
const ENTRYPOINT_GLOB = 'src/main/frontend/**/*.entry.js';
const MAVEN_OUTPUT_DIRECTORY = path.join(__dirname, 'target', 'classes');
const WEBPACK_OUTPUT_SUBDIRECTORY = 'frontend'; // location within target/classes for output
const WEBPACK_FULL_OUTPUT_PATH = path.join(MAVEN_OUTPUT_DIRECTORY, WEBPACK_OUTPUT_SUBDIRECTORY);
const CONFLUENCE_BUNDLED_REACT_BUILD = '10428'; // First build number of Confluence with bundled React (6.15.0-beta2)
const REACT_MODULE = 'devday2019/react'; // Name of published AMD module for react
const REACT_DOM_MODULE = 'devday2019/react-dom'; // Name of published AMD module for react-dom

// Generate one AMD dependency for the WRM-webpack plugin, assuming that the module is exported
// using the same name as we import it here

const providedAmdDep = (importName, resource) => {
    return {
        dependency: resource,
        import: {
            'amd': importName,
            'var': `require("${importName}")`
        }
    };
};

// Generate a web-resource <condition> that is applied only if the Confluence build is lower than a certain
// build number. This is applied to our bundled React and React-DOM libraries so that they do not load
// on versions of the host product where React and React-DOM are already bundled.

const hasBundledReactCondition = {
    'class': 'com.cenotelabs.demo.devday2019.ConfluenceBuildNumberCondition', // note: the build condition built into Confluence seems to be broken in 6.15, so we include our own
    'invert': true,
    'params': [
        {
            'attributes': {
                'name': 'minBuildNumber'
            },
            'value': CONFLUENCE_BUNDLED_REACT_BUILD
        }
    ]
};

// Generate the components of the Webpack-WRM plugin config necessary for generating our library that
// bundles React and React DOM.

const getLibraryWrmConfig = () => {
    return {
        webresourceKeyMap: {
            [REACT_MODULE]: 'reactBundled',
            [REACT_DOM_MODULE]: 'reactDomBundled'
        },
        conditionMap: {
            [REACT_MODULE]: hasBundledReactCondition,
            [REACT_DOM_MODULE]: hasBundledReactCondition
        }
    };
};

// Generates the components of the Webpack-WRM plugin config necessary for building our main app.

const getMainBuildWrmConfig = () => {
    return {
        webresourceKeyMap: {
            // Give our admin.entry.js entrypoint an explicit resource key of "webpackAdminResources"
            'admin.entry': 'webpackAdminResources'
        },
        providedDependencies: {
            // Replace references to 'react' and 'react-dom' with the delegatingReact(Dom)
            // AMD modules, contained within the delegatingReact(Dom) web-resources, which will
            // either give us the host product's bundled React, or our own copy if the host product
            // is not new enough.
            'react': providedAmdDep(REACT_MODULE, `${PLUGIN_KEY}:delegatingReact`),
            'react-dom': providedAmdDep(REACT_DOM_MODULE, `${PLUGIN_KEY}:delegatingReactDom`),

            // Allow us to import these devdayapp2019/xxxx resources by mapping to AMD
            'devdayapp2019/legacyCode': providedAmdDep('devdayapp2019/legacyCode', `${PLUGIN_KEY}:legacyResource`),
            'devdayapp2019/modules/legacyModule': providedAmdDep('devdayapp2019/modules/legacyModule', `${PLUGIN_KEY}:legacyResource`)
        }
    };
};

// Invoke the Atlassian Web Resource Manager plugin to generate the plugin descriptor fragments for us.
// This gets run twice, once for the React library and once for the main plugin.

const webResourcePlugin = (isLibrary) => {
    const baseWrmConfig = {
        pluginKey: PLUGIN_KEY,
        xmlDescriptors: path.join(MAVEN_OUTPUT_DIRECTORY, 'META-INF', 'plugin-descriptors', isLibrary ? 'library.xml' : 'main.xml'),
        locationPrefix: WEBPACK_OUTPUT_SUBDIRECTORY
    };

    return new WrmPlugin(merge(baseWrmConfig, isLibrary ? getLibraryWrmConfig() : getMainBuildWrmConfig()));
};

const excludeNodeModules = () => {
    return (modulePath) => {
        return (/node_modules/.test(modulePath));
    };
};

const flowPlugin = new FlowWebpackPlugin({
    failOnError: false,
    failOnErrorWatch: true,
    verbose: false,
    printFlowOutput: true,
    callback: ({exitCode, stdout, stderr}) => {
        if (exitCode) {
            console.log('Flow result: ' + exitCode);
        }
        return null;
    }
});

const miniCssExtractPlugin =  new MiniCssExtractPlugin({
    filename: "[name].css",
    chunkFilename: "[id].css"
});

const bundleAnalyzerPlugin = (isLibrary) => new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    reportFilename: path.resolve(__dirname, 'target', isLibrary ? 'libraryBundles.html' : 'mainBundles.html'),
    openAnalyzer: false
});

// Plugins for both development and production mode
const commonPlugins = (isLibrary) => [
    webResourcePlugin(isLibrary),
    flowPlugin,
    miniCssExtractPlugin,
    bundleAnalyzerPlugin(isLibrary)
];

// Use these plugins when NODE_ENV=development
const developmentModePlugins = (isLibrary) => [
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({ "process.env.NODE_ENV": JSON.stringify("development") }),
    ...commonPlugins(isLibrary)
];

// Use these plugins when NODE_ENV=production
const productionModePlugins = (isLibrary) => [
    new webpack.DefinePlugin({ "process.env.NODE_ENV": JSON.stringify("production") }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    ...commonPlugins(isLibrary)
];

const plugins = (isLibrary) => PRODUCTION ? productionModePlugins(isLibrary) : developmentModePlugins(isLibrary);

// webpack execution #1:
//
// First run webpack once to bundle React and React-DOM as libraries, accessible via AMD as
// 'devday2019/react' and 'devday2019/react-dom'

const webpackConfigForReactLibrary = {
    name: 'React library',
    context: __dirname,
    mode: 'production', // 'production' here ensures we have a unique UUID for the (unused) assets section, to avoid duplicate keys in plugin XML fragments
    devtool: '',
    entry: {
        [REACT_MODULE]: 'react',
        [REACT_DOM_MODULE]: 'react-dom'
    },
    resolve: {
        modules: ['node_modules'],
    },
    output: {
        library: '[name]',
        libraryTarget: 'amd',
        filename: 'bundled/[name].js',
        path: WEBPACK_FULL_OUTPUT_PATH,
        jsonpFunction: WEBPACK_JSONP_FUNCTION_NAME
    },
    performance: {
        hints: false,
        maxEntrypointSize: 2048000,
        maxAssetSize: 2048000
    },
    optimization: {
        usedExports: false
    },
    plugins: plugins(true)
};

// webpack execution #2:
//
// Bundle our main app

const webpackConfigForMainApp = {
    name: 'Main app',
    context: __dirname,
    mode: 'none', //TODO FIXME: 'production' is needed by WRM plugin to remove pseudo hash for assets. 'development'/'production' -- replaced by plugins section
    devtool: '',
    entry: globEntries(path.resolve(__dirname, ENTRYPOINT_GLOB)),
    output: {
        library: '',
        libraryTarget: 'var',
        filename: '[name].js',
        path: WEBPACK_FULL_OUTPUT_PATH,
        jsonpFunction: WEBPACK_JSONP_FUNCTION_NAME
    },
    optimization: {
        minimize: PRODUCTION,
        minimizer: [ new TerserPlugin({cache: true, parallel: true}) ],
    },
    module: {
        rules: [{
            enforce: 'pre',
            test: [/\.js.flow$/, /\.js$/, /\.jsx$/],
            exclude: excludeNodeModules(),
            use: {
                loader: 'eslint-loader',
                options: {
                    emitError: true
                }
            }
        }, {
            test: [/\.js.flow$/, /\.js$/, /\.jsx$/],
            exclude: excludeNodeModules(),
            use: {
                loader: 'babel-loader',
                options: babelConfig
            }
        }, {
            test: [/\.css$/ ],
            // Alternate: if want inlined CSS in JS:
            //use: ['style-loader', 'css-loader']

            use: [{
                loader: MiniCssExtractPlugin.loader,
                options: {
                    publicPath: '../'
                    }
                },
                'css-loader'
            ]
        },{
            test: /\.(png|jp(e*)g|gif|svg)$/,
            use: {
                loader: 'url-loader',
                options: {
                    limit: 8192,
                    name: 'images/[hash]-[name].[ext]'
                }
            }
        }
        ],
    },
    stats: {
        colors: true,
        chunks: false,
        cachedAssets: false
    },

    plugins: plugins(false)
};

module.exports = [
    webpackConfigForReactLibrary,
    webpackConfigForMainApp
];