###### Developer Day 2019 Demonstration Plugin

This is the example plugin for the Developer Day 2019 presentation titled
"Supercharge your Server Front End with Atlaskit, React, Flow and more".

This plugin demonstrates how to use React and Atlaskit components within a Server
app.

After installing the [Atlassian Plugin SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/downloads/),
you can stand up a development instance of Confluence and automatically install a copy
of this plugin by invoking `atlas-debug`.

After startup, the plugin makes a page available at http://localhost:1990/confluence/admin/devday2019.action
(login/pass `admin`/`admin`) demonstrating the usage of Atlaskit components, webpacked CSS and images, calls to legacy code, and more.

Alternatively, the project can be built using `atlas-mvn package` and the .JAR in the target/ directory
can be uploaded to a standalone Confluence instance, and then accessed using the "DevDay 2019" link in
the sidebar of the Confluence administration section.