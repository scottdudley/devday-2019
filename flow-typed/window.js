// @flow

declare var window: {
    document: Document,

    console: {
        log: (...any) => void
    },

    demoCallToBadGlobalFunction: (string) => string,

    _babelPolyfill?: any
};