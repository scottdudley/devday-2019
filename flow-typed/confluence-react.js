// @flow

declare module 'wr-dependency!react' { // eslint-disable-line no-empty
}

declare module 'wr-dependency!react-dom' { // eslint-disable-line no-empty
}

declare module 'wr-dependency!com.atlassian.confluence.plugins.confluence-frontend:react' { // eslint-disable-line no-empty
}

declare module 'wr-dependency!com.atlassian.confluence.plugins.confluence-frontend:react-dom' { // eslint-disable-line no-empty
}