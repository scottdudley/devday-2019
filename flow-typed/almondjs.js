// @flow

declare function define(name: string, deps?: any[], () => any) : void;
declare function require(...any) : any;
